#
# Cookbook Name:: myhaproxy
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

node.default['haproxy']['members'] = [{
  'hostname' => 'ec2-54-162-205-148.compute-1.amazonaws.com',
  'ipaddress' => '54.162.205.148',
  'port' => 80,
  'ssl_port' => 80
},
{
  'hostname' => 'ec2-107-22-156-95.compute-1.amazonaws.com',
  'ipaddress' => '107.22.156.95',
  'port' => 80,
  'ssl_port' => 80
}]


include_recipe 'haproxy::default'
